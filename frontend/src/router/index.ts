import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import PartnersView from '@/views/PartnersView.vue'
import CriteriaView from '@/views/CriteriaView.vue'
import AccountView from '@/views/AccountView.vue'

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'home',
            component: HomeView
        },
        {
            path: '/partners',
            name: 'partners',
            component: PartnersView
        },
        {
            path: '/criteria',
            name: 'criteria',
            component: CriteriaView
        },
        {
            path: '/account',
            name: 'account',
            component: AccountView
        }
    ]
})

export default router
