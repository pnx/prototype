
export interface Brand {
    Name: string;
    Product: string;
    Category: string;
    Location: string;
}

export interface BrandGroup {
    category: string;
    location: string;

    brands: Brand[]
}