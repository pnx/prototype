package main

import (
	"flag"
	"log"
	"net/http"

	"prototype/app"
	"prototype/app/model"
	"prototype/app/types"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var db *gorm.DB

type BrandSearchParam struct {
	Search string `uri:"search"`
}

func seed() {
	db.Create(&model.Brand{Name: "Starbucks", Product: "Ice latte", Category: types.Coffee, Location: types.Seattle})
	db.Create(&model.Brand{Name: "Mermaidspace", Product: "Microwave", Category: types.Appliances, Location: types.Dallas})
	db.Create(&model.Brand{Name: "Alpinetechs", Product: "Sofa", Category: types.Furniture, Location: types.LosAngeles})
	db.Create(&model.Brand{Name: "Happypoly", Product: "RC Car", Category: types.Toys, Location: types.Miami})
	db.Create(&model.Brand{Name: "Apexway", Product: "Dishwasher", Category: types.Appliances, Location: types.Seattle})
	db.Create(&model.Brand{Name: "Dune", Product: "Americano", Category: types.Coffee, Location: types.Miami})
	db.Create(&model.Brand{Name: "Toys R us", Product: "Trainset", Category: types.Toys, Location: types.Dallas})
	db.Create(&model.Brand{Name: "Dominos", Product: "Pizza", Category: types.Food, Location: types.NewYork})
	db.Create(&model.Brand{Name: "IKEA", Product: "Billy bookshelf", Category: types.Furniture, Location: types.Seattle})
	db.Create(&model.Brand{Name: "Electrolux", Product: "Fridge", Category: types.Appliances, Location: types.Miami})
	db.Create(&model.Brand{Name: "X-Electronics", Product: "Vacuum cleaner", Category: types.Appliances, Location: types.LosAngeles})
	db.Create(&model.Brand{Name: "Littlesprings", Product: "Salad", Category: types.Food, Location: types.Dallas})
	db.Create(&model.Brand{Name: "The Pam", Product: "Chair", Category: types.Furniture, Location: types.NewYork})
	db.Create(&model.Brand{Name: "Funtime", Product: "Teddybear", Category: types.Toys, Location: types.LosAngeles})
	db.Create(&model.Brand{Name: "The Burgerplace", Product: "The Big PC", Category: types.Food, Location: types.LosAngeles})
	db.Create(&model.Brand{Name: "Happy Donut", Product: "Donut", Category: types.Food, Location: types.Seattle})
	db.Create(&model.Brand{Name: "Home Sweet Home", Product: "Electric mixer", Category: types.Appliances, Location: types.NewYork})
	db.Create(&model.Brand{Name: "Lakeview", Product: "Pumpkin Latte", Category: types.Coffee, Location: types.Miami})
	db.Create(&model.Brand{Name: "Home Improvment", Product: "Table", Category: types.Furniture, Location: types.Dallas})
	db.Create(&model.Brand{Name: "Frogster", Product: "Action figure", Category: types.Toys, Location: types.NewYork})
}

func ApiBrandHandler(c *gin.Context) {
	brands := []model.Brand{}

	search := c.Param("search")
	if len(search) > 0 {
		db.Where("name LIKE ?", "%"+search+"%").
			Or("product LIKE ?", "%"+search+"%").
			Or("category LIKE ?", "%"+search+"%").
			Or("location LIKE ?", "%"+search+"%").
			Find(&brands)
	} else {
		db.Find(&brands)
	}

	c.JSON(http.StatusOK, brands)
}

func main() {
	var err error
	db_migration := flag.Bool("dbmigration", false, "")
	db_seed := flag.Bool("dbseed", false, "Seed database")

	config, err := app.LoadConfig("./config.yml")
	if err != nil {
		log.Panic(err)
	}

	flag.Parse()

	db, err = gorm.Open(sqlite.Open(config.DBPath), &gorm.Config{})
	if err != nil {
		log.Panic("failed to connect database", err)
	}

	if *db_migration {
		db.AutoMigrate(&model.Brand{})
		log.Println("migration done")
		return
	}

	if *db_seed {
		seed()
		return
	}

	r := gin.Default()
	r.Use(cors.New(cors.Config{
		AllowOrigins:     config.AllowOrigins,
		AllowMethods:     []string{"GET"},
		AllowHeaders:     []string{"Origin"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
	}))

	// API Routes
	r.GET("/api/brands", ApiBrandHandler)
	r.GET("/api/brands/:search", ApiBrandHandler)

	if err := r.Run(config.Address); err != nil {
		log.Fatalln(err)
	}
}
