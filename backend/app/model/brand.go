package model

import (
	"prototype/app/types"
)

type Brand struct {
	ID       uint `gorm:"primarykey"`
	Name     string
	Product  string
	Category types.Category
	Location types.Location
}
