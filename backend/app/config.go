package app

import (
	"io"
	"os"

	"gopkg.in/yaml.v3"
)

type Config struct {
	Address      string   `yaml:"address"`
	AllowOrigins []string `yaml:"allow_orgins"`
	DBPath       string   `yaml:"db_path"`
}

func LoadConfig(filename string) (*Config, error) {
	cfg := Config{
		Address: ":8080",
		DBPath:  "./db.sqlite",
	}

	file, err := os.Open(filename)
	if err != nil {
		return &cfg, err
	}

	buf, err := io.ReadAll(file)
	if err != nil {
		return &cfg, err
	}

	err = yaml.Unmarshal(buf, &cfg)
	return &cfg, err
}
