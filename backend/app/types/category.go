package types

type Category string

const (
	Coffee     Category = "Coffee"
	Food       Category = "Food"
	Toys       Category = "Toys"
	Appliances Category = "Appliances"
	Furniture  Category = "Furniture"
)
