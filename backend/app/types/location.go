package types

type Location string

const (
	LosAngeles Location = "Los Angeles"
	NewYork    Location = "New York"
	Seattle    Location = "Seattle"
	Dallas     Location = "Dallas"
	Miami      Location = "Miami"
)
