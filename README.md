## backend

Compile

```sh
cd backend
go build cmd/server.go
```

configure

```sh
cp config.example.yml config.yml 
nano config.yml
```

Run with

```sh
./server
```
